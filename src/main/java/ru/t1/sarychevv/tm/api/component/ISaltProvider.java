package ru.t1.sarychevv.tm.api.component;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
