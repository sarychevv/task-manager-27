package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.IUserRepository;
import ru.t1.sarychevv.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return records
                .stream()
                .filter(r -> login.equals(r.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) {
        return records
                .stream()
                .filter(r -> email.equals(r.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return records
                .stream()
                .anyMatch(r -> login.equals(r.getLogin()));
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return records
                .stream()
                .anyMatch(r -> email.equals(r.getEmail()));
    }

}
